from django.shortcuts import render
from django.forms.formsets import formset_factory

from .forms import QuestionForm


def interview(request):
    QuestionFormSet = formset_factory(QuestionForm, extra=5)

    if request.method == "POST":
        formset = QuestionFormSet(request.POST)

        if formset.is_valid():
            message = "Successful Submitted"
            for form in formset:
                form.save()
        else:
            message = "Something went wrong"

        return render(request, 'contact/interview.html',
                      {'message': message})
    else:
        return render(request, 'contact/interview.html',
                      {'formset': QuestionFormSet()})

# Generated by Django 2.0.5 on 2018-06-10 04:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='answered',
            field=models.BooleanField(default=False),
        ),
    ]

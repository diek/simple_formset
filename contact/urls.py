from django.urls import path

from . import views

app = 'contact'

urlpatterns = [
    path('', views.interview, name='interview')
]
